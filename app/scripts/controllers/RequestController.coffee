'use strict'

class RequestController extends Controller
  @$inject: ['$scope']
  constructor: (@scope) ->
    super @scope
  requestInvite: (request)=>
    user = Kinvey.getActiveUser()
    if not user
      Kinvey.User.login "anonymous", "anonymous",
        success: =>
          request._id = request.email
          Kinvey.DataStore.save "requests", request, 
            success: =>
              console.log arguments...
              @scope.message = "Thank you. We will be in touch shortly."
              @scope.$apply()
              Kinvey.User.logout()
            error: =>
              console.log arguments...
              Kinvey.User.logout()
        error: =>
    return

app.controller 'RequestController', RequestController

'use strict'

class MobileFriendRateController extends Controller
  @$inject: ['$scope','$timeout']
  constructor: (@scope,@timeout) ->
    super @scope
    @scope.user = Kinvey.getActiveUser()
    if not @scope.user
      window.location = "/mobile_main.html"
    @scope.info = "Now help your friends discover their hidden strengths. Answer the questions before time runs out. Don't worry this will only take a minute."
    query = new Kinvey.Query()
    query.equalTo "selected", true
    Kinvey.DataStore.find "friends", query,
      success: (response)=>
        console.log response
        @scope.friends = response
        @scope.friendIdx = Math.floor(Math.random()*@scope.friends.length)
        @scope.friend = @scope.friends[@scope.friendIdx]     
        @next()
      error: =>
    # query = new Kinvey.Query()
    # query.notContainedIn "users", @scope.user._socialIdentity.facebook.id
    # if (not @scope.user.questions) or (moment().diff(moment.unix(@scope.user.questionsLoaded), 'days') > 1) 
    #   Kinvey.DataStore.find "questions", query, 
    #     success: (response)=>
    #       @scope.user.questions = response
    #       @scope.user.questionsLoaded = moment().unix()
    #       @updateUser()
    #       @scope.questions = _.clone @scope.user.questions
    #       @init()
    #       @scope.$apply()
    #     error: (err)=>
    # else
    #   @scope.questions = _.clone @scope.user.questions
    #   @init()
  continue: =>
    window.location = "/mobile_load_contacts.html"
  next: =>
    Kinvey.execute "getQuestion",
      fromId: @scope.user._socialIdentity.facebook.id
      toId: @scope.friend.id
    , 
      success: (response)=>
        console.log response
        data = response.data
        data.question = data.question.replace /%Name/, @scope.friend.name
        # @scope.user._socialIdentity.facebook.name
        @timeout =>
          console.log response
          @scope.cur = +response.total - +response.count
          @scope.count = response.total
          @scope.question = data
      error: (err)=>
        @timeout =>
          @scope.success = "Congratulation! You're done."
          @scope.question = null
  skip: (q)=>
    @scope.info = ""
  answer: (friend, q, choice)=>
    console.log arguments...
    @scope.info = ""
    strength =
      owner: friend.id
      time: moment().unix()
    if choice.attributes
      for attr in choice.attributes
        strength[attr.attribute.toLowerCase()] = +attr.weight
    console.log JSON.stringify strength
    Kinvey.execute "answerQuestion",
      fromId: @scope.user._socialIdentity.facebook.id
      toId: friend.id
      qid: q._id
      answer: choice.choice
      strength: strength
    ,
      success: =>
        @next()
      error: =>
        @next()
    return
app.controller 'MobileFriendRateController', MobileFriendRateController

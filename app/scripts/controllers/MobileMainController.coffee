'use strict'

class MobileMainController extends Controller
  @$inject: ['$scope', 'KinveyLoginService', '$timeout']
  constructor: (@scope,@KinveyLoginService,@timeout) ->
    super @scope
  connectFacebook: =>
    @KinveyLoginService.connect @scope, 'facebook', (err, response)=>
      @scope.user = Kinvey.getActiveUser()
      facebookIdentity = @scope.user._socialIdentity.facebook;
      if not @scope.user.picture 
        @scope.user.picture = "http://graph.facebook.com/#{facebookIdentity.id}/picture"
        @updateUser()
      if (not @scope.user.friendsLoaded)# or (moment().diff(moment.unix(@scope.user.friendsLoaded), 'days') > 1) 
        @scope.loading = "Loading your contacts..." 
        FB.api 'me/friends','GET', access_token: facebookIdentity.accessToken, (response)=>
          friends = response.data
          console.log friends
          @scope.user.friendsLoaded = moment().unix()
          @updateUser =>
            async.each friends, (friend, cb)=>
              friend._id = friend.id
              friend.picture = "http://graph.facebook.com/#{friend.id}/picture"
              # cb()
              Kinvey.DataStore.get "friends", friend._id, 
                success: (response)=>
                  @timeout =>
                    @scope.loading = "Loading #{friend.name}"
                  , 0          
                  # acl = new Kinvey.Acl(response)
                  # acl.addWriter(@scope.user._id);
                  # acl.addReader(@scope.user._id);
                  # if(Object.keys(response).length == Object.keys(friend).length)
                  #   cb()
                  #   return
                  _.merge response, friend
                  Kinvey.DataStore.update "friends", response,
                    success: =>
                      @timeout =>
                        @scope.loading = "#{friend.name} Updated"
                      , 0 
                      cb()
                    error: =>
                      @timeout =>
                        @scope.loading = "#{friend.name} Error"
                      , 0 
                      cb()
                error: =>
                  @timeout =>
                    @scope.loading = "Loading #{friend.name}"
                  , 0
                  Kinvey.DataStore.save "friends", friend,
                    success: =>
                      @timeout =>
                        @scope.loading = "#{friend.name} Loaded"
                      , 0 
                      cb()
                    error: =>
                      @timeout =>
                        @scope.loading = "#{friend.name} Error"
                      , 0
                      cb()
            , =>
              @timeout =>
                @scope.loading = "Done"
                window.location = "/mobile_self_rate.html"
              ,0
      else
        window.location = "/mobile_self_rate.html"
          # @scope.loading = ""
          # shuffle(friends)
          # for friend in friends
          #   friend.thumbnail_url = "http://graph.facebook.com/#{friend.id}/picture?type=large"
          #   friend.questions = questions
          # $scope.userdata.allfriends = friends
          # $scope.userdata.friendsloaded = true
          # ref = new Firebase("https://prestie.firebaseio.com/friends/#{$scope.userdata.id}")
          # ref.set friends, ->
          #   $timeout ->
          #     $scope.$broadcast 'pt:ready'
          #   , 0
app.controller 'MobileMainController', MobileMainController

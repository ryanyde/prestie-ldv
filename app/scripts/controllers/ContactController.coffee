'use strict'

class ContactController extends Controller
  @$inject: ['$scope']
  constructor: (@scope) ->
    super @scope
  sendContact: (contact)=>
    user = Kinvey.getActiveUser()
    if not user
      Kinvey.User.login "anonymous", "anonymous",
        success: =>
          Kinvey.execute "sendEmail", contact, 
            success: =>
              console.log arguments...
              @scope.message = "Thank you for contacting us. We will be in touch soon."
              @scope.$apply()
              Kinvey.User.logout()
            error: =>
              console.log arguments...
              Kinvey.User.logout()
        error: =>

    return
app.controller 'ContactController', ContactController

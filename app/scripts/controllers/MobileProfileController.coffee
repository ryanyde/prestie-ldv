'use strict'

class MobileProfileController extends Controller
  @$inject: ['$scope', '$timeout', '$http']
  constructor: (@scope,@timeout,@http) ->
    super @scope
    @scope.user = Kinvey.getActiveUser()
    if not @scope.user
      window.location = "/mobile_main.html"
  charge: (type)=>
    pay = _.once (token, args) =>
      console.log arguments...
      Kinvey.execute "action",
        action: "charge"
        amount: 99
        card: token.id
        currency: "usd"
        metadata:
          id: @scope.user._id
          facebook: 
            id: @scope.user._socialIdentity.facebook.id
            name:  @scope.user._socialIdentity.facebook.name
          type: type or "premium"
      ,
        success: =>
          console.log arguments...
        error: =>
          console.log arguments...
    handler = StripeCheckout.configure(
      key: "pk_test_b999p8U7DqDLr9TTMbZZ3hof"
      image: "/img/favicon.png"
      token: pay
    )
    handler.open
      name: "Prestie"
      description: "Prestie"
      amount: 99

app.controller 'MobileProfileController', MobileProfileController

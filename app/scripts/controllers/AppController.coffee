'use strict'

class AppController extends Controller
  @$inject: ['$scope']
  constructor: (@scope) ->
    super @scope

app.controller 'AppController', AppController

'use strict'

class FooterController extends Controller
  @$inject: ['$scope']
  constructor: (@scope) ->
    super @scope
  requestNews: (email)=>
    user = Kinvey.getActiveUser()
    if not user
      Kinvey.User.login "anonymous", "anonymous",
        success: =>
          Kinvey.DataStore.save "news", {email:email, _id: email}, 
            success: =>
              console.log arguments...
              @scope.message = "Thank you for signing up."
              @scope.$apply()
              Kinvey.User.logout()
            error: =>
              console.log arguments...
              Kinvey.User.logout()
        error: =>

    return

app.controller 'FooterController', FooterController

'use strict'

class HeaderController extends Controller
  @$inject: ['$scope']
  constructor: (@scope) ->
    super @scope
  
app.controller 'HeaderController', HeaderController

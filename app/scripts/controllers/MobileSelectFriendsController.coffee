'use strict'

class MobileSelectFriendsController extends Controller
  @$inject: ['$scope', '$timeout']
  constructor: (@scope,@timeout) ->
    super @scope
    @scope.user = Kinvey.getActiveUser()
    if not @scope.user
      window.location = "/mobile_main.html"
    @scope.info = "Now pick some friends to rate.  Swipe right to remove from the list.  Tap to select and deselect.  Don't worry, every response here is completely and totally anonymous."
    query = new Kinvey.Query()
    query.ascending("name")
    # @KinveyCollectionService.bind @scope, "friends", "friends", query
    Kinvey.DataStore.find "friends", query,
      success: (friends)=>
        @timeout =>
          @scope.friends = friends
    
    @scope.per_page = 20
    @scope.page = 1
  #   @loading = false
  #   $(window).scroll =>
  #     #End of page, load next content here
  #     @loadNextPage()  unless @loading  if $(window).scrollTop() >= $(document).height() - $(window).height() - 100
  # loadNextPage: =>
  #   console.log "load"
  #   @loading = true
  #   @loading = false

  delete: (index)=>
    friend = @scope.friends.splice index, 1
    friend.deleted = true
    friend.dirty = true
    # Kinvey.DataStore.destroy "friends", friend[0]._id
    # @KinveyCollectionService.sync @scope, "friends", "friends"
  select: (friend)=>
    if friend.deleted
      return
    friend.selected = not friend.selected
    friend.dirty = true
    # @KinveyCollectionService.sync @scope, "friends", "friends"
    # Kinvey.DataStore.update "friends", angular.copy(friend)
  done: =>
    async.each @scope.friends, (friend,cb)=>
      if not friend.dirty
        cb()
        return
      if friend.deleted
        Kinvey.DataStore.destroy "friends", friend[0]._id,
          success: =>
            cb()
          error: =>
            cb()
      else
        Kinvey.DataStore.update "friends", angular.copy(friend),
          success: =>
            cb()
          error: =>
            cb()
    , =>
      window.location = "/mobile_rate_friends.html"
  updateUser:(cb=->)=>
    Kinvey.User.update @scope.user,
      success: (response)=>
        cb null, response 
      error: (err)=>
        cb err
  # updateUser:(cb=->)=>
  #   @KinveyUserService.update @scope, 'user', (err, response)=>
  #     cb err, response  
app.controller 'MobileSelectFriendsController', MobileSelectFriendsController

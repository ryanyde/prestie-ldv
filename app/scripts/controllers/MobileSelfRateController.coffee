'use strict'

class MobileSelfRateController extends Controller
  @$inject: ['$scope', '$timeout']
  constructor: (@scope,@timeout) ->
    super @scope
    @scope.user = Kinvey.getActiveUser()
    if not @scope.user
      window.location = "/mobile_main.html"
    console.log @scope.user
    @scope.info = "Start by rating yourself. Answer the questions before time runs out. Don't worry this will only take a minute."
    @next()
    # query = new Kinvey.Query()
    # query.notContainedIn "users", @scope.user._socialIdentity.facebook.id
    # if (not @scope.user.questions) or (moment().diff(moment.unix(@scope.user.questionsLoaded), 'days') > 1) 
    #   Kinvey.DataStore.find "questions", query, 
    #     success: (response)=>
    #       @scope.user.questions = response
    #       @scope.user.questionsLoaded = moment().unix()
    #       @updateUser()
    #       @scope.questions = _.clone @scope.user.questions
    #       @init()
    #       @scope.$apply()
    #     error: (err)=>
    # else
    #   @scope.questions = _.clone @scope.user.questions
    #   @init()
  continue: =>
    window.location = "/mobile_select_friends.html"
  next: =>
    Kinvey.execute "getQuestion",
      self: true
      fromId: @scope.user._socialIdentity.facebook.id
      toId: @scope.user._socialIdentity.facebook.id
    , 
      success: (response)=>
        data = response.data
        @scope.cur = response.total - response.count
        @scope.count = response.total
        data.question = data.question.replace /%Name/, "you"
        # @scope.user._socialIdentity.facebook.name
        @timeout =>
          @scope.question = data
      error: (err)=>
        @timeout =>
          @scope.success = "Congratulation! You're done."
          @scope.question = null
  skip: (q)=>
    @scope.info = ""
  answer: (q,choice)=>
    @scope.info = ""
    strength =
      owner: @scope.user._socialIdentity.facebook.id
      time: moment().unix()
    console.log choice.attributes
    if choice.attributes
      for attr in choice.attributes
        strength[attr.attribute.toLowerCase().replace(/\s/,"")] = +attr.weight
    Kinvey.execute "answerQuestion",
      self: true
      fromId: @scope.user._socialIdentity.facebook.id
      toId: @scope.user._socialIdentity.facebook.id
      qid: q._id
      answer: choice.choice
      strength: strength
    ,
      success: =>
        @next()
      error: =>
        @next()
    return

app.controller 'MobileSelfRateController', MobileSelfRateController

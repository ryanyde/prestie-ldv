app.factory 'KinveyLoginService', ['$rootScope',($rootScope)->
  return  {
    signup: ($scope,user, cb=->)->
      Kinvey.User.signup user,
        success: (response)->
          cb(null, response)
          $scope.$apply()
        error: (err)->
          cb(err)
          $scope.$apply()
    login: ($scope,user, cb=->)-> 
      Kinvey.User.login user.username, user.password,
        success: (response)->
          cb(null, response)
          $scope.$apply()
        error: (err)->
          cb(err)
          $scope.$apply()
    connect: ($scope,type,cb=->)-> 
      doConnect = ->
        Kinvey.Social.connect null, type,
          success: (response)->
            cb(null, response)
            $scope.$apply()
          error: (err)->
            cb(err)
            $scope.$apply()
      Kinvey.User.logout
        success: ->
          doConnect()
        error: ->
          doConnect()
  }
]
app.factory 'KinveyUserService', ['$rootScope',($rootScope)->
  return  {
    get: ($scope, name, id, cb=->)->
      Kinvey.User.get id,
        success: (response)->
          $scope[name] = response
          $scope.$apply ->
            cb null, response
        error: ->
          $scope.$apply ->
            cb arguments...
    destroy: ($scope, name, cb=->)->
      obj = $scope[name]
      Kinvey.User.destroy obj._id,
        success: (response)->
          $scope.$apply ->
            cb null, response          
        error: (err)->
          $scope.$apply ->
            cb arguments...
    create: ($scope, name, cb=->)->
      obj = $scope[name]
      Kinvey.User.save angular.copy(obj),
        success: (response)->
          $scope[name] = response
          $scope.$apply ->
            cb null, response          
        error: (err)->
          $scope.$apply ->
            cb arguments...
    update: ($scope, name, cb=->)->
      obj = $scope[name]
      Kinvey.User.update angular.copy(obj),
        success: (response)->
          $scope[name] = response
          $scope.$apply ->
            cb null, response
        error: (err)->
          $scope.$apply ->
            cb arguments...   
  }
]
app.factory 'KinveyService', ['$rootScope',($rootScope)->
  return  {
    get: ($scope, name, collections, id, cb=->)->
      Kinvey.DataStore.get collections, id,
        success: (response)->
          $scope[name] = response
          $scope.$apply ->
            cb null, response
        error: ->
          cb arguments...
    destroy: ($scope, name, collections, cb=->)->
      obj = $scope[name]
      Kinvey.DataStore.destroy collections, obj._id,
        success: (response)->
          $scope.$apply ->
            cb null, response          
        error: (err)->
          cb arguments...
    create: ($scope, name, collections, cb=->)->
      obj = $scope[name]
      Kinvey.DataStore.save collections, angular.copy(obj),
        success: (response)->
          $scope[name] = response
          $scope.$apply ->
            cb null, response          
        error: (err)->
          cb arguments...
    update: ($scope, name, collections, cb=->)->
      obj = $scope[name]
      Kinvey.DataStore.update collections, angular.copy(obj),
        success: (response)->
          $scope[name] = response
          $scope.$apply ->
            cb null, response
        error: (err)->
          cb arguments...   
  }
]
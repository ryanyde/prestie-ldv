app.factory 'KinveyCollectionService', ->
  return  {
    bind: ($scope, name, collections, query=(new Kinvey.Query()), cb=->)->
      Kinvey.DataStore.find collections, query,
        success: (response)->
          # console.log collections, response
          $scope.ids?={}
          $scope.ids[name] = _.pluck response, "_id"
          $scope[name] = response
          $scope.$apply ->
            for k,obj of $scope[name]
              do (k,obj)->
                $scope.$watch "#{name}.#{k}", (o,n)->
                  for k,v of o                    
                    if k == "$$hashKey"
                      continue
                    if v != n[k]                      
                      obj.dirty = true
                      break
                , true
            cb null, response
          # console.log scope.educations
          # successFn arguments...
        error: ->
          cb arguments...

    sync: ($scope, name, collections)->
      ids = {}
      for i,id of $scope.ids[name]
        ids[id] = i
      for obj,k in $scope[name]
        do (k,obj)->
          delete ids[obj._id]
          if obj.dirty == true and obj._kmd
            delete obj.dirty
            console.log "update"
            Kinvey.DataStore.update collections, angular.copy(obj)
          else if not obj._kmd
            Kinvey.DataStore.save collections, angular.copy(obj),
              success: (response)->
                for k,v of response
                  obj[k] = v
                $scope.ids[name].push response._id
                $scope.$apply()
      for k,v of ids
        do (k,v)->
          $scope.ids[name].splice v, 1
          Kinvey.DataStore.destroy collections, k

  }

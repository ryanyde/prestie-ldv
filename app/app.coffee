'use strict'

# Declare app level module which depends on filters, and services

window.app = angular.module('app', [
	# 'ngRoute'
  'ngTouch'
	'ngAnimate'
	'partials'
])

# app.config([
# 	'$routeProvider'
# 	'$locationProvider'
# 	($routeProvider, $locationProvider, config) ->
# 		$routeProvider
# 			.when('/', {templateUrl: '/partials/main.html'})
# 			.otherwise({redirectTo: '/'})

# 		# Without server side support html5 must be disabled.
# 		$locationProvider.html5Mode(false)
# ])

init = ->
	#do some initialization here

window.initialized = false
class Controller
  constructor: (@scope) ->
    for k in _.functions @
      @scope[k] = @[k] if k!="constructor"
    if window.initialized
      return
    window.initialized = true
    init()
  updateUser:(cb=->)=>
    Kinvey.User.update @scope.user,
      success: (response)=>
        cb null, response 
      error: (err)=>
        cb err

# bootstrap angular
angular.element(document).ready ->
  promise = Kinvey.init
    appKey: 'kid_VV4Dw5Jeji',
    appSecret: '37512bc7b3f94eb0b5d53e72b92c79ed'
  promise.then -> 
    angular.bootstrap document, ['app']
    window.callFunction();
    map_initialize();